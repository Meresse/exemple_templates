
#include <iostream>
using namespace std;


class Dog
{
protected:

	string voice() const
	{
		return "woof !";
	}
};


class Cat
{
protected:

	string voice() const
	{
		return "meow !";
	}
};


class Hamster
{
protected:

	string voice() 
	{
		return "squeak !";
	}
};


template<class T>
class Animal : public T
{
public:
	
	void voice()
	{
		cout << T::voice() << endl;
	}
};


int main()
{
	Animal<Dog> A;
	Animal<Cat> B;
	Animal<Hamster> C;
	
	A.voice();
	B.voice();
	C.voice();
}




